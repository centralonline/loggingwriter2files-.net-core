﻿using System;
using System.Collections.Generic;
using System.Text;
using OfficeMate.LoggingWriter2Files.Internal;

namespace OfficeMate.LoggingWriter2Files
{
    /// <summary>
    /// Options for file logging.
    /// </summary>
    public class FileLoggerOptions : BatchingLoggerOptions
    {
        private int? _fileSizeLimit = 10 * 1024 * 1024;
        private int? _retainedFileCountLimit = 10;
        private string _fileName = "logs-";

        private string _extensionName = "txt";
        private bool? _fileGroupingEnabled = true;
        private bool? _fileSequenceEnabled = true;
        private bool? _fileSizeLimitEnabled = true;

        /// <summary>
        /// Gets or sets a strictly positive value representing the maximum log size in bytes or null for no limit.
        /// Once the log is full, no more messages will be appended.
        /// Defaults to <c>10MB</c>.
        /// </summary>
        public int? FileSizeLimit
        {
            get { return _fileSizeLimit; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), $"{nameof(FileSizeLimit)} must be positive.");
                }
                _fileSizeLimit = value;
            }
        }

        /// <summary>
        /// Gets or sets a boolean value representing the named log size grouping by Year, Month Day.
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool? FileGroupingEnabled
        {
            get { return _fileGroupingEnabled; }
            set
            {
                if (!value.HasValue)
                {
                    throw new ArgumentException(nameof(value), $"{nameof(FileGroupingEnabled)} must be declare.");
                }
                _fileGroupingEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a boolean value representing the named log size grouping by Year, Month Day and Sequence if full size.
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool? FileSequenceEnabled
        {
            get { return _fileSequenceEnabled; }
            set
            {
                if (!value.HasValue)
                {
                    throw new ArgumentException(nameof(value), $"{nameof(FileSequenceEnabled)} must be declare.");
                }
                _fileSequenceEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a boolean value representing the log size in bytes for no limit.
        /// Defaults to <c>true</c>.
        /// </summary>
        public bool? FileSizeLimitEnabled
        {
            get { return _fileSizeLimitEnabled; }
            set
            {
                if (!value.HasValue)
                {
                    throw new ArgumentException(nameof(value), $"{nameof(FileSizeLimitEnabled)} must be declare.");
                }
                _fileSizeLimitEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a strictly positive value representing the maximum retained file count or null for no limit.
        /// Defaults to <c>2</c>.
        /// </summary>
        public int? RetainedFileCountLimit
        {
            get { return _retainedFileCountLimit; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), $"{nameof(RetainedFileCountLimit)} must be positive.");
                }
                _retainedFileCountLimit = value;
            }
        }

        /// <summary>
        /// Gets or sets the filename prefix to use for log files.
        /// Defaults to <c>logs-</c>.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(nameof(value));
                }
                _fileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the file extension for log files.
        /// Defaults to <c>txt</c>.
        /// </summary>
        public string ExtensionName
        {
            get { return _extensionName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(nameof(value));
                }
                _extensionName = value;
            }
        }

        /// <summary>
        /// The directory in which log files will be written, relative to the app process.
        /// Default to <c>Logs</c>
        /// </summary>
        /// <returns></returns>
        public string LogDirectory { get; set; } = "logs";
    }
}
