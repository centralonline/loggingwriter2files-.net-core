﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace OfficeMate.LoggingWriter2Files.Internal
{
    public class BatchingLogger : ILogger
    {
        private readonly BatchingLoggerProvider _provider;
        private readonly string _category;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public BatchingLogger(BatchingLoggerProvider loggerProvider, string categoryName, IHttpContextAccessor httpContextAccessor)
        {
            _provider = loggerProvider;
            _category = categoryName;

            _httpContextAccessor = httpContextAccessor;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            if (logLevel == LogLevel.None)
            {
                return false;
            }
            return true;
        }

        public void Log<TState>(DateTimeOffset timestamp, LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            var builder = new StringBuilder();

            builder.Append(eventId.Id.ToString());
            builder.Append(" ");
            builder.Append(timestamp.ToString("yyyy-MM-dd HH:mm:ss.fff zzz"));
            builder.Append(" ");

            if (_httpContextAccessor?.HttpContext != null)
            {
                var _traceIdentifierId = _httpContextAccessor.HttpContext.TraceIdentifier;
                if (_traceIdentifierId.IndexOf(_httpContextAccessor.HttpContext.Connection.Id) != -1)
                    _traceIdentifierId = string.Empty;

                builder.Append(_traceIdentifierId);
                builder.Append(" ");
                builder.Append(_httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString());
                builder.Append(" ");
            }

            builder.Append("[");
            builder.Append(logLevel.ToString());
            builder.Append("] ");
            builder.Append(_category);
            builder.Append(": ");
            builder.AppendLine(formatter(state, exception));

            if (exception != null)
            {
                builder.AppendLine(exception.ToString());
            }

            _provider.AddMessage(timestamp, builder.ToString());
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            Log(DateTimeOffset.Now, logLevel, eventId, state, exception, formatter);
        }
    }
}
