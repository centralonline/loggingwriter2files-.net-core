﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OfficeMate.LoggingWriter2Files.Internal
{
    public struct LogMessage
    {
        public DateTimeOffset Timestamp { get; set; }
        public string Message { get; set; }
    }
}
