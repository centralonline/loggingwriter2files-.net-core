﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;

namespace OfficeMate.LoggingWriter2Files
{
    public static class IdentifierLoggerExtensions
    {
        public static IApplicationBuilder UseIdentifierLogger(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseMiddleware<IdentifierLoggerMiddleware>();
        }
    }
}
