﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OfficeMate.LoggingWriter2Files.Internal;
using Microsoft.AspNetCore.Http;

namespace OfficeMate.LoggingWriter2Files
{
    /// <summary>
    /// An <see cref="ILoggerProvider" /> that writes logs to a file
    /// </summary>
    [ProviderAlias("File")]
    public class FileLoggerProvider : BatchingLoggerProvider
    {
        private readonly string _path;
        private readonly string _fileName;
        private readonly int? _maxFileSize;
        private readonly int? _maxRetainedFiles;

        private readonly string _extensionName;
        private readonly bool? _fileGroupingEnabled;
        private readonly bool? _fileSequenceEnabled;
        private readonly bool? _fileSizeLimitEnabled;

        /// <summary>
        /// Creates an instance of the <see cref="FileLoggerProvider" /> 
        /// </summary>
        /// <param name="options">The options object controlling the logger</param>
        public FileLoggerProvider(IOptions<FileLoggerOptions> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
            var loggerOptions = options.Value;
            _path = loggerOptions.LogDirectory;
            _fileName = loggerOptions.FileName;
            _maxFileSize = loggerOptions.FileSizeLimit;
            _maxRetainedFiles = loggerOptions.RetainedFileCountLimit;

            _extensionName = loggerOptions.ExtensionName;
            _fileGroupingEnabled = loggerOptions.FileGroupingEnabled;
            _fileSequenceEnabled = loggerOptions.FileSequenceEnabled;
            _fileSizeLimitEnabled = loggerOptions.FileSizeLimitEnabled;
        }

        /// <inheritdoc />
        protected override async Task WriteMessagesAsync(IEnumerable<LogMessage> messages, CancellationToken cancellationToken)
        {
            Directory.CreateDirectory(_path);

            if (_fileGroupingEnabled.Value)
            {
                foreach (var group in messages.GroupBy(GetGrouping))
                {
                    var fullName = GetFullName(group.Key);
                    var fileInfo = new FileInfo(fullName);
                    if (_fileSizeLimitEnabled.Value && _maxFileSize > 0 && fileInfo.Exists && fileInfo.Length > _maxFileSize)
                    {
                        if (!_fileSequenceEnabled.Value)
                            return;

                        int seq = 1;
                        bool hasName = false;
                        while (!hasName)
                        {
                            var _group = (group.Key.Year, group.Key.Month, group.Key.Day, seq);
                            var _fullName = GetFullName(_group);
                            var _fileInfo = new FileInfo(_fullName);
                            if (_maxFileSize > 0 && _fileInfo.Exists && _fileInfo.Length > _maxFileSize)
                            {
                                seq++;
                            }
                            else
                            {
                                hasName = true;
                                fullName = _fullName;
                                fileInfo = _fileInfo;
                            }
                        }
                    }

                    using (var streamWriter = File.AppendText(fullName))
                    {
                        foreach (var item in group)
                        {
                            await streamWriter.WriteAsync(item.Message);
                        }
                    }
                } 
            }
            else
            {
                var fullName = GetFullName();
                var fileInfo = new FileInfo(fullName);
                if (_fileSizeLimitEnabled.Value && _maxFileSize > 0 && fileInfo.Exists && fileInfo.Length > _maxFileSize)
                {
                    return;
                }

                using (var streamWriter = File.AppendText(fullName))
                {
                    foreach (var item in messages)
                    {
                        await streamWriter.WriteAsync(item.Message);
                    }
                }
            }

            RollFiles();
        }

        private string GetFullName()
        {
            //return Path.Combine(_path, $"{_fileName}.txt");
            return Path.Combine(_path, $"{_fileName}.{_extensionName}");
        }

        private string GetFullName((int Year, int Month, int Day) group)
        {
            //return Path.Combine(_path, $"{_fileName}{group.Year:0000}{group.Month:00}{group.Day:00}.txt");
            return Path.Combine(_path, $"{_fileName}{group.Year:0000}{group.Month:00}{group.Day:00}.{_extensionName}");
        }

        private string GetFullName((int Year, int Month, int Day, int seq) group)
        {
            //return Path.Combine(_path, $"{_fileName}{group.Year:0000}{group.Month:00}{group.Day:00}-{group.seq}.txt");
            return Path.Combine(_path, $"{_fileName}{group.Year:0000}{group.Month:00}{group.Day:00}-{group.seq}.{_extensionName}");
        }

        private (int Year, int Month, int Day) GetGrouping(LogMessage message)
        {
            return (message.Timestamp.Year, message.Timestamp.Month, message.Timestamp.Day);
        }

        /// <summary>
        /// Deletes old log files, keeping a number of files defined by <see cref="FileLoggerOptions.RetainedFileCountLimit" />
        /// </summary>
        protected void RollFiles()
        {
            if (_maxRetainedFiles > 0)
            {
                var files = new DirectoryInfo(_path)
                    .GetFiles(_fileName + "*")
                    .OrderByDescending(f => f.Name)
                    .Skip(_maxRetainedFiles.Value);

                foreach (var item in files)
                {
                    item.Delete();
                }
            }
        }
    }
}
