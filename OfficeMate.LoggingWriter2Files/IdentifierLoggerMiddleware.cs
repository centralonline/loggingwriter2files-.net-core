﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace OfficeMate.LoggingWriter2Files
{
    public class IdentifierLoggerMiddleware
    {
        private readonly RequestDelegate _next;

        public IdentifierLoggerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context?.TraceIdentifier != null)
                context.TraceIdentifier = Guid.NewGuid().ToString();

            await _next(context);
        }

    }
}
