# Version

- 2.0.0
  > Write the Logger EventId  
  > Write the client IP Address  
  > Write the TraceIdentifierId (guid) same exception error

- 1.0.5
  > .NET Core version 2.0.0  
  > .NET Core Logging version 2.0.1

  > *Configured extension name*  
  > *Created grouping and sequence files*  
  > *Create one or multiple file and file size no limit*

# Configuration

- **LogDirectory**
  > *Default = logs*  
  > ระบุ Path สำหรับเก็บ Log Files

- **FileName**
  > *Default = logs-*  
  > *Full Name :*	กรณี FileGroupingEnabled = false  
  > *Prefix Name :* กรณี FileGroupingEnabled = true

- **ExtensionName**
  > *Default = txt*  
  > ระบุ Extension Name โดยไม่ต้องมี "."

- **FileGroupingEnabled**
  > *Default = true*  
  > *True :* เปิดการใช้งานจัดเก็บไฟล์แบบ Grouping -> yyyyMMdd (yyyyMMdd-n, ...)  
  > *False :* ปิดการใช้งาน

- **FileSizeLimit**
  > *Default = 10MB*  

- **FileSizeLimitEnabled**
  > *Default = true*  
  > *True :* เปิดการใช้งาน File Size Limit โดยการกำหนด *FileSizeLimit*  
  > *False :* ปิดการใช้งาน

- **RetainedFileCountLimit**
  > *Default = 10*  
  > ระบุจำนวนไฟล์ที่เก็บย้อนหลัง โดยเรียงตาม Latest Date

# Solution 1
#### จัดเก็บแบบไฟล์เดียว ไม่มี Size Limit
```
FileName = "log-api";
ExtensionName = "log";
FileGroupingEnabled = false;
FileSizeLimitEnabled = false;
```

# Solution 2
#### จัดเก็บแบบไฟล์เดียว และมี Size Limit
```
FileName = "log-api";
ExtensionName = "log";
FileGroupingEnabled = false;
FileSizeLimit = 100 * 1024 * 1024;  // 100MB
```

# Solution 3
#### จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd ไม่มี Size Limit
```
//FileName = "logs-";
ExtensionName = "log";
FileSizeLimitEnabled = false;
RetainedFileCountLimit = 10;        // เก็บย้อนหลังล่าสุด 10 ไฟล์
```

# Solution 4
#### จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd และมี Size Limit
```
//FileName = "logs-";
ExtensionName = "log";
FileSequenceEnabled = false;
FileSizeLimit = 100 * 1024 * 1024;  // 100MB
RetainedFileCountLimit = 10;        // เก็บย้อนหลังล่าสุด 10 ไฟล์
```

# Solution 5
#### จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd แบบมี Sequence และมี Size Limit
##### *yyyyMMdd-n, ... กรณี Full Size*
```
//FileName = "logs-";
ExtensionName = "log";
FileSizeLimit = 100 * 1024 * 1024;  // 100MB
RetainedFileCountLimit = 10;        // เก็บย้อนหลังล่าสุด 10 ไฟล์
```

# Example
#### Program.cs
```
public class Program
{
  public static void Main(string[] args)
  {
    BuildWebHost(args).Run();
  }

  public static IWebHost BuildWebHost(string[] args) =>
    WebHost.CreateDefaultBuilder(args)
      .ConfigureLogging(builder => builder.AddFile(options => {
        options.ExtensionName = "log";
        options.FileSizeLimit = 100 * 1024 * 1024;
        options.RetainedFileCountLimit = 10;
      }))
      .UseStartup<Startup>()
      .Build();
}
```

#### Startup.cs
##### *Override .NET TraceIdentifier with GUID*
```
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
  app.UseIdentifierLogger();
  app.UseMvc();
}
```