﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace OfficeMate.LoggingWriter2Files.Sample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging(builder => builder.AddFile(options => {

                    // Solution 1 : จัดเก็บแบบไฟล์เดียว ไม่มี Size Limit
                    //options.FileName = "log-sample-api";
                    //options.ExtensionName = "log";
                    //options.FileGroupingEnabled = false;
                    //options.FileSizeLimitEnabled = false;

                    // Solution 2 : จัดเก็บแบบไฟล์เดียว และมี Size Limit
                    //options.FileName = "log-sample-api";
                    //options.ExtensionName = "log";
                    //options.FileGroupingEnabled = false;
                    //options.FileSizeLimit = 1024 * 1024 * 1024;   // 1GB

                    // Solution 3 : จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd ไม่มี Size Limit
                    ////options.FileName = "logs-";
                    //options.ExtensionName = "log";
                    //options.FileSizeLimitEnabled = false;
                    //options.RetainedFileCountLimit = 10;          // เก็บย้อนหลังล่าสุด 10 ไฟล์

                    // Solution 4 : จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd และมี Size Limit
                    ////options.FileName = "logs-";
                    //options.ExtensionName = "log";
                    //options.FileSequenceEnabled = false;
                    //options.FileSizeLimit = 100 * 1024 * 1024;    // 100MB
                    //options.RetainedFileCountLimit = 10;          // เก็บย้อนหลังล่าสุด 10 ไฟล์

                    // Solution 5 : จัดเก็บแบบจัดกลุ่มไฟล์ตาม yyyyMMdd แบบมี Sequence และมี Size Limit
                    // yyyyMMdd-n, ... กรณี Full Size
                    ////options.FileName = "logs-";
                    options.ExtensionName = "log";
                    options.FileSizeLimit = 100 * 1024 * 1024;      // 100MB
                    options.RetainedFileCountLimit = 10;            // เก็บย้อนหลังล่าสุด 10 ไฟล์

                }))
                .UseStartup<Startup>()
                .Build();
    }
}
