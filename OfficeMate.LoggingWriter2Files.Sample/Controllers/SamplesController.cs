﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeMate.LoggingWriter2Files.Sample.Models;

namespace OfficeMate.LoggingWriter2Files.Sample.Controllers
{
    [Produces("application/json")]
    [Route("api/Samples")]
    public class SamplesController : Controller
    {
        private readonly ILogger<SamplesController> _logger;

        public SamplesController(ILogger<SamplesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }

        [HttpGet("{id}", Name = "GetById")]
        public IActionResult Get(int id)
        {
            var objData = new SampleModel();
            objData.Id = 1;
            objData.Code = "01";
            objData.Name = "Abc";

            return Ok(objData);
        }

        [HttpPost]
        public IActionResult Post([FromBody] SampleModel objData)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("ModelState Invalid!");
                return BadRequest(ModelState);
            }

            return CreatedAtRoute("GetById", new { id = objData.Id }, objData);
        }

        [HttpPut("{id}")]
        public IActionResult Post(int id, [FromBody] SampleModel objData)
        {
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Post(int id)
        {
            return Ok();
        }
    }
}