﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace OfficeMate.LoggingWriter2Files.Sample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Use(async (context, next) =>
                {
                    var error = context.Features[typeof(IExceptionHandlerFeature)] as IExceptionHandlerFeature;
                    if (error != null && error.Error != null)
                    {
                        //var feature = HttpContext.Features.Get<IHttpRequestIdentifierFeature>();
                        //var feature = app.ApplicationServices.GetRequiredService<Microsoft.AspNetCore.Http.Features.IHttpRequestIdentifierFeature>();
                        //var identifier = feature.TraceIdentifier;
                        var httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();

                        context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                        {
                            Code = "ERR0X000001",
                            Message = error.Error.Message,
                            StackTrace = error.Error.StackTrace,
                            TraceIdentifierId = httpContextAccessor.HttpContext.TraceIdentifier
                        }));
                    }
                    else
                    {
                        await next();
                    }
                });
            });

            app.UseIdentifierLogger();

            app.UseMvc();
        }
    }
}
